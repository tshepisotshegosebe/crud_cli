<?php
namespace CrudTerminal\Controllers;

use CrudTerminal\commandPrompts;
use CrudTerminal\CrudOperations;
use CrudTerminal\Exceptions\StudentNotFoundException;
use CrudTerminal\Models\Student;
use CrudTerminal\Services\JsonService;
use CrudTerminal\Services\ValidationService;

class CrudController implements CrudOperations
{
    use commandPrompts;

    Const COMMAND_ADD_STUDENT = 'add';
    Const COMMAND_EDIT_STUDENT = 'edit';
    Const COMMAND_DELETE_STUDENT = 'delete';
    Const COMMAND_SEARCH_STUDENT = 'search';

    /**
     * add student to $this->student array and make use of saveJson in JsonService
     * to save an object of student to json file
     */
    public function addStudent(): void
    {
        try {
            foreach (['Id', 'Name', 'Surname', 'Age', 'Curriculum'] as $field) {
                $this->commandAddPrompt($field);
            }
            $student = new Student($this->student);
            (new JsonService($student))->saveJson();
            echo "{$student->getFullName()} with id: $student->id saved successfully";

        }catch (\InvalidArgumentException | \ErrorException $e){
            echo 'Caught exception: ',  $e->getMessage();
        }
        echo PHP_EOL;
        exit();
    }

    /**
     * filter results by inserted input
     */
    public function searchStudent(): void
    {
        $headerArray = array_keys(get_object_vars( new Student([])));
        echo "Enter Search criteria: ";
        $response = fopen ("php://stdin","r");
        $val = str_replace("\n", "", fgets($response));
        $searchValues = explode('=',preg_replace('/\s/', '', $val)); //remove white spaces and explode the input

        if(count($searchValues) >= 3) { // if it has more than 2 elements then its wrong, remind how to use it
            (new ValidationService())->filterUsage();
        }
        if (strlen(reset($searchValues)) >= 2 && strlen(end($searchValues)) >= 1){ // if both search results meet requirements
            if (!in_array(strtolower(reset($searchValues)),$headerArray)){ //then check if inserted field exist in student object
                echo 'field: '.reset($searchValues).' does not exist';
                echo PHP_EOL;
                die();
            }
        }

        commandPrompts::createHeadings(); //create heading
        if (count($searchValues) == 1){
            $this->getSearchResults($searchValues, true);
        }else{
            $this->getSearchResults($searchValues, false);
        }

        echo PHP_EOL;
    }

    /**
     * edit student
     *
     * @param string $id
     */
    public function editStudent(string $id): void
    {
        echo 'Leave field blank to keep previous value'.PHP_EOL;

        try {
            if (!isset($this->existingStudents[$id])){
                throw new StudentNotFoundException("Student with Id: $id does not exist");
            }
            $oldStudentData = $this->existingStudents[$id]; //search from all students by id
            $this->student['id'] = $oldStudentData->id; //assign old id to new array before saving

            foreach (['Name', 'Surname', 'Age', 'Curriculum'] as $field) { // exclude id
                $existingField = $oldStudentData->{strtolower($field)};
                $this->commandEditPrompt($field, $existingField);
            }
            $newStudentData = new Student($this->student);
            (new JsonService($newStudentData))->editJson(); //delete and recreate json file
            echo "Student with id: $oldStudentData->id updated successfully";
        }catch (\InvalidArgumentException | \ErrorException | StudentNotFoundException $e){
            echo 'Caught exception: ',  $e->getMessage();
        }
        echo PHP_EOL;
        exit();
    }

    /**
     * delete student by removing json file
     * @param int $id
     */
    public function deleteStudent(string $id): void
    {
        $message = "This operation is irreversible, Are you sure you want continue? [y/N]";
        print $message;

        $confirmation = trim(fgets(STDIN));
        if ($confirmation !== 'y') {
            exit();
        }

        try{
            if (!isset($this->existingStudents[$id])){
                throw new StudentNotFoundException("Student with Id: $id does not exist");
            }
            $student = $this->existingStudents[$id];
            (new JsonService($student))->deleteJson(); //delete json file
            echo "Student with id: $student->id deleted successfully";
        }catch(\ErrorException | \InvalidArgumentException | StudentNotFoundException $e){
            echo 'Caught exception: ',  $e->getMessage();
        }
        echo PHP_EOL;
        exit();
    }
}