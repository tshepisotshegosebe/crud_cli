<?php

namespace CrudTerminal;

interface CrudOperations
{
    public function addStudent(): void;
    public function searchStudent(): void;
    public function editStudent(string $id): void; //$id should be string to keep leading zeros so dont cast
    public function deleteStudent(string $id): void; //$id should be string to keep  leading zeros so dont cast
}