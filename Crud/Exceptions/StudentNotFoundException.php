<?php
namespace CrudTerminal\Exceptions;

use Exception;

class StudentNotFoundException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $this->message = strlen($message) < 0 ? "could not find student" : $message;
        parent::__construct($this->message, $code, $previous);
    }

}