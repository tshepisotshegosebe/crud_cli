<?php
namespace CrudTerminal\Models;

class Student
{
    public $id;
    public $name;
    public $surname;
    public $age;
    public $curriculum;

    public function __construct(array $data = []){
        $this->id = isset($data['id']) ? $data['id']: null ;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->surname = isset($data['surname']) ? $data['surname'] : null;
        $this->age = isset($data['age']) ? (int)$data['age']: null;
        $this->curriculum = isset($data['curriculum']) ? $data['curriculum']: null;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return "{$this->name} {$this->surname}";
    }

    /**
     * @return string
     */
    public function fileLocation(): string
    {
        $subDir = substr((string)$this->id, 0, 2);

        return "Students/$subDir/{$this->id}.json";
    }
}