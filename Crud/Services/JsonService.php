<?php
namespace CrudTerminal\Services;
use CrudTerminal\Models\Student;

class JsonService
{
    /**
     * @var Student
     */
    private $student;

    public function __construct(Student $student = null)
    {
        $this->student = $student;
    }

    // save to json file
    public function saveJson(): bool
    {
        $firstDir = substr($this->student->id, 0, 2);
        $path = "Students/$firstDir/";
        if (!file_exists($path)) { // check if path exist, then create if if not
            mkdir($path, 0777, true);
        }
        $file = fopen("$path/{$this->student->id}.json",'w');
        $write = fwrite($file, json_encode($this->student, JSON_PRETTY_PRINT));
        if ($write == false){
            throw new \ErrorException('could not write to json file');
        }
        fclose($file);

        return true;
    }

    /**
     *
     * basically just delete and recreate json file
     * @return bool
     * @throws \ErrorException
     */
    public function editJson(): bool
    {
        $this->deleteJson();

        return $this->saveJson();
    }

    /**
     * check if file is writable and remove it
     *
     * @throws \ErrorException
     */
    public function deleteJson(): void
    {
        $file = BASEPATH.'/'.$this->student->fileLocation();
        if (is_writable($file)){
            unlink($file);
        }else{
            throw new \ErrorException("could not delete $file, because it is not writable");
        }

    }

    /*
     * fetch all student from sub directories and read inner json
     * supply basecontroller with it so i alwys have them in my controller
     */
    public function fetchExistingStudents(): array
    {
        $existingStudents = [];
        $path    = BASEPATH.'/Students';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $folders = array_diff(scandir($path), ['.', '..']);
        foreach ($folders as $folder) {
            $files = array_diff(scandir("$path/$folder"), ['.', '..']);
            foreach ($files as $file) {
                $student = json_decode(file_get_contents("$path/$folder/$file"));
                if ($student == NULL) continue;
                $existingStudents[$student->id] = new Student((array)$student);
            }
        }

        return  $existingStudents;
    }
}