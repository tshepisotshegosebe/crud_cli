<?php

namespace CrudTerminal\Services;

class ValidationService
{
    private $existingStudents;

    public function __construct($existingStudents = false)
    {
        $this->existingStudents = $existingStudents;
    }

    /**
     * @param      $input
     * @param bool $checkExistence
     */
    public function validateId($input, bool $checkExistence = true)
    {
        if ($checkExistence && array_key_exists((int)$input, (array) $this->existingStudents)){
            throw new \InvalidArgumentException('Id Should be unique, Student Id: '.$input.' exist and belongs to '.$this->existingStudents[$input]->getFullName().''. PHP_EOL);
        }elseif(strlen($input) !== 7){
            $message = 'Id should consist of 7 digits';
            if($checkExistence){
                throw new \InvalidArgumentException('Id should consist of 7 digits');
            }else{
                echo $message.''.PHP_EOL;
                die();
            }
        }elseif (ctype_digit($input) === false){
            throw new \InvalidArgumentException('Id should consists of numbers only');
        }

    }

    /**
     * validate all the inputs inserted when adding student
     *
     * @param string $input
     * @param string $field
     */
    public function validateInputs(string $input, string $field)
    {
        switch ($field) {
            case 'Id':
                $this->validateId($input);
                break;
            case 'Surname':
            case 'Name':
                $this->validateNames($input);
                break;
            case 'Age':
                $this->validateAge($input);
                break;
            case 'Curriculum':
                $this->validateCurriculum($input);
                break;
        }
    }

    /**
     * how to of the terminal application returned if command is wrong
     */
    public static function commandUsage()
    {
        $commandUsage = 'Usage: php main.php [option]=[command]'.PHP_EOL;
        $commandUsage .= 'Options:'.PHP_EOL;
        $commandUsage .= '--action:    Passes the command, needed all the time'.PHP_EOL;
        $commandUsage .= '--id:   Only needed when --action=[edit,delete]'.PHP_EOL;
        $commandUsage .= PHP_EOL;
        $commandUsage .= 'Command:'.PHP_EOL;
        $commandUsage .= 'add:   Add new student'.PHP_EOL;
        $commandUsage .= 'edit:   Edit existing student'.PHP_EOL;
        $commandUsage .= 'delete:   Delete existing student'.PHP_EOL;
        $commandUsage .= 'search:   Filter existing student'.PHP_EOL;

        echo $commandUsage;
        echo PHP_EOL;
        die();
    }

    public static function filterUsage()
    {
        $commandUsage = 'Usage: php main.php [field]=[value]'.PHP_EOL;
        $commandUsage .= 'e.g: Enter Search criteria: age=55 | Curriculum=English | name=john | surname=DoE'.PHP_EOL;
        echo $commandUsage;
        echo PHP_EOL;
        die();
    }

    /**
     * @param $input
     */
    private function validateNames($input)
    {
        if (strlen($input) < 3){
            throw new \InvalidArgumentException('Field should be 3 or more character long');
        }elseif (ctype_alpha($input) === false){
            throw new \InvalidArgumentException('Field should consists of letters only');
        }
    }

    /**
     * @param $input
     */
    private function validateAge($input)
    {
        if (strlen($input) > 2){
            throw new \InvalidArgumentException('Age should be 2 or less character long');
        }elseif(ctype_digit($input) === false){
            throw new \InvalidArgumentException('Age should consists of numbers only');
        }
    }

    /**
     * @param $input
     */
    private function validateCurriculum($input)
    {
        if (strlen($input) < 3){
            throw new \InvalidArgumentException('Name should be 3 or more character long');
        }
    }
}