<?php

namespace CrudTerminal;

use CrudTerminal\Models\Student;
use CrudTerminal\Services\JsonService;
use CrudTerminal\Services\ValidationService;

trait commandPrompts {

    protected $student = [];
    protected $existingStudents;

    public function __construct()
    {
        $this->existingStudents = (new JsonService())->fetchExistingStudents(); // get all existing student from json files
    }

    /**
     * create heading with formatted spaces
     */
    public static function createHeadings(): void
    {
        echo '----------------------------------------------------------------------------------------'.PHP_EOL;
        $format = "|%-10s |%-20s |%-20s |%-5s |%-20s\n";
        printf($format, 'Id', 'Name', 'Surname', 'Age', 'Curriculum');
        echo '----------------------------------------------------------------------------------------'.PHP_EOL;
    }

    /**
     * a prompt function asking for user input and validating before saving to json file
     *
     * @param string $value
     */
    private function commandAddPrompt(string $value): void
    {
        echo "Enter Student $value: ";
        $response = fopen ("php://stdin","r");
        $val = str_replace("\n", "", fgets($response));
        (new ValidationService($this->existingStudents))->validateInputs($val, $value);
        $this->student[strtolower($value)] = $val; //add to $this array
    }

    /**
     * @param string $value
     * @param string $existingValue
     */
    private function commandEditPrompt(string $value, string $existingValue): void
    {
        echo "Enter Student [$existingValue] $value: ";
        $response = fopen ("php://stdin","r");
        $val = str_replace("\n", "", fgets($response));
        if(strlen($val) > 0){ //if input is inserted for change then validate it
            (new ValidationService($this->existingStudents))->validateInputs($val, $value);
        }
        $this->student[strtolower($value)] = strlen($val) > 0 ? $val : $existingValue; //if nothing is inserted then give it the old value
    }

    /**
     * filter the search and show the table body from filtered result
     *
     * @param array $searchCriteria
     * @param bool  $showAll
     */
    private function getSearchResults(array $searchCriteria, bool $showAll = false): void
    {
        $results = [];
        $field = strtolower(reset($searchCriteria));
        $value = strtolower(end($searchCriteria));


        if (!$showAll && strlen($field) >= 2){
            foreach ($this->existingStudents as $existingStudent) {
                if (strtolower($existingStudent->{$field}) == $value){ // lower everything, both the field and value to match them and overcome case sensitivity
                    $results[] = $existingStudent; //add to result if matches
                }
            }
        }

        $results = $showAll ? $this->existingStudents : $results; // just show all if showAll is true

        /** @var Student $student */
        foreach ($results  as $student){
            $format = "|%-10s |%-20s |%-20s |%-5s |%-20s\n";
            printf($format, $student->id, $student->name, $student->surname, $student->age, $student->curriculum);
            echo '----------------------------------------------------------------------------------------'.PHP_EOL;
        }
    }
}
