# Terminal CRUD (command line application)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Project is created with: `PHP >=7.2`, `Composer` and `PHP Ant` build tool

### Installing
- clone the [project](https://bitbucket.org/tshepisotshegosebe/cli-crud/src/master/).
- Run Ant built to install Composer, make sure your are inside the root directory.

```
ant full-build
```
``full-build`` is the name of the target as you can see below from `build.xml`
```xml
<target name="full-build" depends="composer"
    description="download the Composer installer, verify its signature, and run the installer.">
    <echo message="Built"/>
</target>
```

wait for ``BUILD SUCCESSFUL``.


## Commands
Commands | Description
:------|------------:
``php main.php --action=add`` |Add new student.
``php main.php --action=search`` | Filter stored student by criteria
``php main.php --action=delete --id=1234567`` | Delete Student with id =`1234567`
``php main.php --action=edit --id=1234567`` | Edit Student with id =`1234567`


## Authors

[**Tshepiso Tshegosebe**](https://www.linkedin.com/in/tshepiso-tshegosebe-a39a64116/)

