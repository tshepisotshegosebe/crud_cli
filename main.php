<?php

use CrudTerminal\Controllers\CrudController;
use CrudTerminal\Services\ValidationService;

require __DIR__.'/vendor/autoload.php';
define('BASEPATH', __DIR__);

$crudController = new CrudController();
$commands = getopt(null, ["action:","id:"]);
$validateService = new ValidationService();
if (count($commands) == 0) ValidationService::commandUsage();
$actionCommand = strtolower($commands['action']);
switch ($actionCommand) {
    case $crudController::COMMAND_ADD_STUDENT:
        $crudController->addStudent();
        break;
    case $crudController::COMMAND_EDIT_STUDENT: //if command = edit then look for id
        $commands['id'] ?? ValidationService::commandUsage();
        if (strlen($commands['id']) !== 7) $validateService->validateId($commands['id'], false);
        $crudController->editStudent($commands['id']);
        break;
    case $crudController::COMMAND_DELETE_STUDENT:  //if command = delete then look for id
        $commands['id'] ?? ValidationService::commandUsage();
        if (strlen($commands['id']) !== 7) $validateService->validateId($commands['id'], false);
        $crudController->deleteStudent($commands['id']);
        break;
    case $crudController::COMMAND_SEARCH_STUDENT:
        $crudController->searchStudent();
        break;
    default:
        ValidationService::commandUsage();
        break;
}